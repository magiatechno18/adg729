/*
 * Multiplexer driver for Analog Devices ADG729 Double 4:1 mux
 *
 * Copyright (C) 2021 MagIA diagnostics
 *
 * Author: Damien KIRK <damien.kirk@magia-diagnostics.com>
 *
 * Written using the driver for ADG792 Triple 4:1 mux, by Peter Rosin.
 *
 * Caution: while the device is technically a matrix switch, I am using
 * it as a double 4:1 multiplexer.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 */

#include <linux/err.h>
#include <linux/i2c.h>
#include <linux/module.h>
#include <linux/mux/driver.h>
#include <linux/property.h>
#include <linux/types.h>
#include <linux/pm_runtime.h>

#define ADG729_MAX_STATES 4
#define FIELD_SET(data, mask, shift, value) \
    (((data) & (~(mask))) | (((value) << (shift)) & (mask)))
#define LOW_MASK 0x0f
#define LOW_SHIFT 0
#define HIGH_MASK 0xf0
#define HIGH_SHIFT 4

/* Just reminding that the state is a 0-based enumeration, i.e. 0-3 for this 4-way multiplexer */
static int adg729_set(struct mux_control *mux, int state) {
    struct i2c_client *i2c = to_i2c_client(mux->chip->dev.parent);
    uint8_t cmd;
    int32_t smbus_status = 0x24;
    uint8_t status;
    uint8_t tries = 0;

    if (state >= ADG729_MAX_STATES || state < MUX_IDLE_DISCONNECT) {
        dev_err(&mux->chip->dev, "Cannot assign state %d, maximum state is %d\n", state, ADG729_MAX_STATES - 1);
    }

#ifdef CONFIG_PM_SLEEP
    if (state >= 0)
        pm_runtime_get_sync(&mux->chip->dev);
#endif

    if (state == MUX_IDLE_AS_IS) {
        dev_info(&mux->chip->dev, "Mux idle as is. Not changing state... \n");
#ifdef CONFIG_PM_SLEEP
        pm_runtime_put(&mux->chip->dev);
#endif
        return 0;
    }

#ifndef ADG729_I2C_SPOOF
    tries = 0;
    smbus_status = i2c_smbus_read_byte(i2c);

    while (-EAGAIN == smbus_status && tries++ < 4) {
        smbus_status = i2c_smbus_read_byte(i2c);
    }

    dev_info(&mux->chip->dev, "SMBus status : %u", smbus_status);

    if (smbus_status < 0)
        return smbus_status;
#endif
    status = smbus_status;

    if (mux->chip->controllers == 1) {
        /* Controlling both muxes at the same time. */
        if (state == MUX_IDLE_DISCONNECT)
            cmd = 0x00;
        else
            cmd = (1 << (state)) | (1 << (state + ADG729_MAX_STATES));
    } else {
        unsigned int controller = mux_control_get_index(mux);

        if (state == MUX_IDLE_DISCONNECT) {
            cmd = FIELD_SET(status, controller ? HIGH_MASK : LOW_MASK, controller ? HIGH_SHIFT : LOW_SHIFT, 0);
        } else {
            cmd = FIELD_SET(status, controller ? HIGH_MASK : LOW_MASK, controller ? HIGH_SHIFT : LOW_SHIFT, 0);
            cmd |= (1 << (state + controller * ADG729_MAX_STATES));
        }
    }

    dev_info(&mux->chip->dev, "Sending byte 0x%x to the mux\n", cmd);
#ifndef ADG729_I2C_SPOOF
    tries = 0;
    smbus_status = i2c_smbus_write_byte(i2c, cmd);
    while (-EAGAIN == smbus_status && tries++ < 4) {
        smbus_status = i2c_smbus_write_byte(i2c, cmd);
    }

    dev_info(&mux->chip->dev, "SMBus status : %u", smbus_status);
#endif

#ifdef CONFIG_PM_SLEEP
    if (state == MUX_IDLE_DISCONNECT) {
        pm_runtime_put(&mux->chip->dev);
    }
#endif

#ifndef ADG729_I2C_SPOOF
    return smbus_status;
#else
    return 0;
#endif
}

static const struct mux_control_ops adg729_ops = {
    .set = adg729_set,
};

static int adg729_suspend(struct device *dev) {
    dev_info(dev, "Suspend\n");
    return 0;
}

static int adg729_resume(struct device *dev) {
    dev_info(dev, "Resume\n");
    return 0;
}

#ifdef CONFIG_PM_SLEEP
static const struct dev_pm_ops adg729_pm_ops = {
    SET_SYSTEM_SLEEP_PM_OPS(adg729_suspend, adg729_resume)
    SET_RUNTIME_PM_OPS(adg729_suspend, adg729_resume, NULL)
};
#endif

static int adg729_probe(struct i2c_client *i2c, const struct i2c_device_id *id) {
    struct device* dev = &i2c->dev;
    struct mux_chip* mux_chip;
    int ret = 0;
    int i;
    uint32_t cells = 0;
    int32_t idle_state[2];

    ret = device_property_read_u32(dev, "#mux-control-cells", &cells);
    if (ret < 0)
        return ret;
    if (cells >= 2)
        return -EINVAL;

    /* The #mux-control-cells indicate here if we want to control both channels at the same
     * time, with #mux-control-cells = 0, or if we want to control them separately, with
     * #mux-control-cells = 1 */
    mux_chip = devm_mux_chip_alloc(dev, cells ? 2 : 1, 0);
    if (IS_ERR(mux_chip))
        return PTR_ERR(mux_chip);

    mux_chip->ops = &adg729_ops;

    /* At boot, the ADG729 has all channels open, so we don't need to reset it to a known state.
     * We may do it anyways. */

    /* If the idle-state property was provided in the device tree, we set the idle_state values
     * accordingly. Otherwise, we set them to the default "as-is". */
    ret = device_property_read_u32_array(dev, "idle-state",
                                         (u32 *)idle_state,
                                         mux_chip->controllers);

    if (ret < 0) {
        idle_state[0] = MUX_IDLE_AS_IS;
        idle_state[1] = MUX_IDLE_AS_IS;
    }

    /* We set the idle state for each of the mux controllers (1 or 2 depending on the decision to control
     * each channel separately). */
    for (i = 0; i < mux_chip->controllers; i++) {
        struct mux_control *mux = &mux_chip->mux[i];
        mux->states = ADG729_MAX_STATES;
        switch (idle_state[i]) {
        case MUX_IDLE_DISCONNECT:
        case MUX_IDLE_AS_IS:
        case 0 ... (ADG729_MAX_STATES - 1):
            mux->idle_state = idle_state[i];
            break;
        default:
            dev_err(dev, "invalid idle-state %d\n", idle_state[i]);
            return -EINVAL;
        }
    }

    ret = devm_mux_chip_register(dev, mux_chip);
    if (ret < 0)
        return ret;

    if (cells)
        dev_info(dev, "Two *single-pole, quadruple throw* muxes registered.\n");
    else
        dev_info(dev, "One *double-pole, quadruple throw* mux registered.\n");

#ifdef CONFIG_PM_SLEEP
    pm_runtime_get_noresume(&i2c->dev);
    pm_runtime_set_active(&i2c->dev);
    pm_runtime_enable(&i2c->dev);
    pm_runtime_put(&i2c->dev);
#endif
    return 0;
}

static int adg729_remove(struct i2c_client* client) {
    dev_info(&client->dev, "Removing ADG729\n");
#ifdef CONFIG_PM_SLEEP
    pm_runtime_disable(&client->dev);
#endif
    return 0;
}

static const struct i2c_device_id adg729_id[] = {
{ .name = "adg729", },
{ /* sentinel */ }
};
MODULE_DEVICE_TABLE(i2c, adg729_id);

static const struct of_device_id adg729_of_match[] = {
{ .compatible = "adi,adg729" },
{ /* sentinel */}
};
MODULE_DEVICE_TABLE(of, adg729_of_match);

static struct i2c_driver adg729_driver = {
    .driver     = {
        .name = "adg729",
        .of_match_table = of_match_ptr(adg729_of_match),
    #ifdef CONFIG_PM_SLEEP
        .pm = &adg729_pm_ops,
    #endif
    },
    .probe      = adg729_probe,
    .remove     = adg729_remove,
    .id_table   = adg729_id,
};
module_i2c_driver(adg729_driver);

/*
static int __init adg729_driver_init(void) {
    return i2c_add_driver(&adg729_driver);
}
late_initcall(adg729_driver_init);

static void __exit adg729_driver_exit(void) {
    return i2c_del_driver(&adg729_driver);
}
module_exit(adg729_driver_exit);
*/
MODULE_DESCRIPTION("Analog Devices ADG729 Double 4:1 mux driver");
MODULE_AUTHOR("Damien KIRK <damien.kirk@magia-diagnostics.com>");
MODULE_LICENSE("GPL v2");
